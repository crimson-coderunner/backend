from flask_sockets import Sockets
from flask import render_template, request, url_for, redirect, Blueprint
from flask import Flask
app = Flask(__name__)
sockets = Sockets(app)

ws = Blueprint('ws', __name__)