import os
import sys
import threading

from flask import (Flask, abort, flash, make_response, redirect,
                   render_template, request, url_for)
from werkzeug.utils import secure_filename

from app import app
from config import get_config, get_supported_languages, set_config
from dal import (check_backstage_session, docker_build, docker_buildall,
                 docker_get_container_list, docker_get_server_info,
                 docker_langjson_load, docker_save_dockerfile, edit_user,
                 get_user, get_users, lang_delete_action,
                 lang_save_action_package, langjson_new_action, list_path,
                 login_backstage, logout_backstage, modify_user_password,
                 new_user, remove_user, sandbox_get_iostream, toggle_user_ban,
                 get_user_workspace, list_path)
from util import password_generate_hash

app.secret_key = os.urandom(16)

THREAD = None
THREAD_LOCK = threading.Lock()

TO_CONTAINER_SOCKET = None
FROM_CONTAINER_STREAM = None

FRONTEND_TYPE = 'new_'

@app.route('/backstage')
def backstage_root():
    return redirect(url_for('backstage_login'))

def login_required(f):
    def res(*x):
        admin_name, session_id = request.cookies.get('admin_name'), request.cookies.get('admin_session_id')
        if check_backstage_session(admin_name, session_id):
            return f(*x)
        else:
            abort(403)
    return res



############# WEB FRONTEND...
@login_required
@app.route("/backstage/main")
def backstage_main():
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        config=get_config()
    )

## login.
@app.route("/backstage/login", methods=["GET", "POST"])
def backstage_login():
    
    if request.method == 'GET':
        return render_template('backstage/login.html.jinja', config=get_config())
    else:
        if valid_login(request.form['username'], request.form['password']):
            session_id = login_backstage(request.form['username'])
            resp = redirect(url_for('backstage_main'))
            resp.set_cookie('admin_name', request.form['username'])
            resp.set_cookie('admin_session_id', session_id)
            return resp
        else:
            return render_template('backstage/login.html.jinja', error='Login failed', config=get_config())

def valid_login(username, password):
    config = get_config()
    return username in config['admin-list'] and config['admin-list'][username] == password

################################################# USERS

@login_required
@app.route('/backstage/users')
def backstage_users():
    user_list = get_users()
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='users',
        user_list=user_list,
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        config=get_config()
    )


@login_required
@app.route('/backstage/users/new', methods=['GET', 'POST'])
def backstage_users_new():
    if request.method == 'GET':
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='users', submode='new',
            ADMIN_USERNAME=request.cookies.get('admin_name'), 
            config=get_config()
        )
    else:
        new_user(request.form['username'], request.form['password'])
        return redirect(url_for('backstage_users'))
    

@login_required
@app.route('/backstage/users/ban/<id>')
def backstage_users_ban(id):
    toggle_user_ban(userid=id)
    return redirect(url_for('backstage_users'))


@login_required
@app.route('/backstage/users/dirtree/<id>')
def backstage_users_dirtree(id):
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='users',
        submode='dirtree',
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        user_dirtree=list_path(userid=id),
        config=get_config()
    )


@login_required
@app.route('/backstage/users/edit/<id>', methods=['GET', 'POST'])
def backstage_users_edit(id):
    if request.method == 'GET':
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='users',
            submode='edit',
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            user=get_user(userid=id),
            config=get_config()
        )
    else:
        edit_user({
            'uid': id,
            'upass': password_generate_hash(request.form['password']),
            'blocked': 1 if request.form['banned'] else 0
        })
        print('edit_complete')
        return redirect(url_for('backstage_users'))



AUTHENTICATED = False

@login_required
@app.route('/backstage/users/delete/<id>')
def backstage_users_delete(id):
    remove_user(userid=id)
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='users', user_list=get_users(),
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        config=get_config()
    )


@login_required
@app.route('/backstage/user/dirtree/<id>')
def backstage_users_workspace(id):
    u = get_user(userid=id)
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='users',
        submode='dirtree',
        USERID=id,
        WORKSPACE_PATH=get_user_workspace(u['uname']),
        USERNAME=u['uname'],
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        CONTENT=list_path(username=u['uname']),
        config=get_config()
    )

################################################# SANDBOX

@login_required
@app.route('/backstage/sandbox')
def backstage_sandbox():
    args = {
        'mode': 'sandbox',
        'server_info': docker_get_server_info(),
        'config': get_config()
    }
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja', **args)
    
@login_required
@app.route('/backstage/lang', methods=['GET', 'POST'])
def backstage_lang():
    if request.method == 'GET':
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='lang',
            supported_language=docker_langjson_load(),
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            config=get_config()
        )
    else:
        docker_save_dockerfile(request.form['lang_name'], request.files['file'])
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='lang',
            actionmsg='Update done.',
            supported_language=docker_langjson_load(),
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            config=get_config()
        )

@login_required
@app.route('/backstage/lang/edit/<lang_name>', methods=['GET', 'POST'])
def backstage_lang_edit(lang_name):
    print('LOG>> lang_edit')
    if request.method == 'GET':
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='lang',
            submode='edit',
            action_list=docker_langjson_load()[lang_name],
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            lang_name=lang_name,
            config=get_config()
        )
    else:
        group, action_name = request.form['group'], request.form['action_name']
        name = request.form['name']
        fileobj = request.files['file']
        langjson_new_action(lang_name, group, action_name, name)
        lang_save_action_package(lang_name, group, action_name, fileobj)
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='lang',
            submode='edit',
            actionmsg='Action created.',
            action_list=docker_langjson_load()[lang_name],
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            config=get_config()
        )

@login_required
@app.route('/backstage/lang/action/remove/<lang>/<group>/<id>', methods=['GET', 'POST'])
def backstage_lang_actionremove(lang, group, id):
    lang_delete_action(lang, group, id)
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='lang',
        submode='edit',
        actionmsg='Action removed.',
        action_list=docker_langjson_load()[lang],
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        config=get_config()
    )

@login_required
@app.route('/backstage/lang/build_all')
def backstage_lang_buildall():
    docker_buildall()
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='lang',
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        actionmsg='Build All started. This might take a long while...',
        config=get_config()
    )
    # TODO: complete this.
    #     

@login_required
@app.route('/backstage/lang/new_action', methods=['POST'])
def backstage_lang_newaction():
    return ''
    # TODO: complete this.

@login_required
@app.route('/backstage/lang/build/<lang_name>/<group>/<action>')
def backstage_lang_build(lang_name, group, action):
    docker_build(lang_name, group, action)
    return render_template(
        FRONTEND_TYPE + 'backstage/main.html.jinja',
        mode='lang',
        ADMIN_USERNAME=request.cookies.get('admin_name'),
        actionmsg='Building image. this might take a while.',
        supported_language=docker_langjson_load(),
        action_list=docker_langjson_load()[lang_name],
        config=get_config()
    )

# TODO:
# 1. change the method to one volume per user.
# 2. 

################################################# CONFIG

@login_required
@app.route('/backstage/config', methods=['GET', 'POST'])
def backstage_config():
    if request.method == 'GET':
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='config',
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            config=get_config()
        )
    elif request.method == 'POST':
        updated = {x: request.form[x] for x in request.form if x != 'type'}
        set_config(request.form['type'], updated)
        return render_template(
            FRONTEND_TYPE + 'backstage/main.html.jinja',
            mode='config',
            actionmsg='Saved.',
            ADMIN_USERNAME=request.cookies.get('admin_name'),
            config=get_config()
        )
    else:
        abort(500)

@login_required
@app.route('/backstage/logout')
def backstage_logout():
    flash('Logout success.')
    logout_backstage(request.cookies.get('admin_name'))
    resp = redirect(url_for('backstage_login'))
    resp.delete_cookie('admin_name')
    resp.delete_cookie('admin_session_id')
    return resp
