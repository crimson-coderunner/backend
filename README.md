CodeRunner
==========

## Requirements

+ Python 3.7
  - flask
  - argon2 (for password hashing)
  - flask_sockets (for websocket server)
+ gunicorn
  - gevent
+ docker
+ MariaDB/MySQL
+ Redis
+ Some stuff for deprecated frontend:
  - npm

## Setup

1. Install:
   + MariaDB
   + Redis
   + Docker
2. Install these packages using pip:
   + gunicorn
   + gevent
   + flask
   + argon2
   + flask_sockets
3. Set up the database using `mysql_create_table.sql`
4. ~~If you want to use the deprecated frontend as a fallback option:~~

   ```
   # cd ./static
   # npm install
   ```

   You can still use the old backstage dashboard, but the old
   frontend is deprecated before it was completed. Use [this](https://gitlab.com/crimson-coderunner/frontend)
   instead.
5. Read `./config/config.json` for configuration.
6. Back to the root of the repository, then `gunicorn -k flask_sockets.worker main:app`.

## Languages

Currently only bash is provided.
