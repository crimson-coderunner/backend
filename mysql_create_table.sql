-- uses mysql/mariadb.

DROP TABLE IF EXISTS Users;
CREATE TABLE IF NOT EXISTS Users (
    uid INT NOT NULL AUTO_INCREMENT,
    uname VARCHAR(64) NOT NULL UNIQUE,
    upass VARCHAR(113) NOT NULL,
    volume_id VARCHAR(64),
    blocked BOOLEAN,
    PRIMARY KEY (uid)
);
