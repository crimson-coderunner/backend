# some utilities.

import os
import re
import base64
import argon2
import random
import tarfile
import zipfile

# returns 'salt:hash'
def password_generate_hash(x):
    salt = os.urandom(16)
    hashed = argon2.argon2_hash(x, salt, buflen=64)
    return f'{base64.b64encode(salt).decode()}:{base64.b64encode(hashed).decode()}'

def password_check_hash(password, hashcode):
    salt, hashed = hashcode.split(':')
    decoded_salt = base64.b64decode(salt)
    decoded_hash = base64.b64decode(hashed)
    return argon2.argon2_hash(password, decoded_salt, buflen=64) == decoded_hash


RANDOM_NAME_CHAR_CHOICE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

def align_name(name, length=8, random_choice=RANDOM_NAME_CHAR_CHOICE):
    return name[:length] if len(name) >= length \
      else f'{name}{"".join(random.sample(random_choice, length - len(name)))}'

def new_name(name, prefix_len=8, length=24):
    if length < 16:
        raise Exception('Length must be at least 16.')
    return align_name(f'{align_name(name, length=prefix_len-1)}_', length=length)

def append_random_postfix(name, postfix_len=8):
    return f'{name}{base64.b16encode(os.urandom(postfix_len//4*3)).decode()}'

REGEX_ESCAPE_NEWLINE_1 = re.compile('\r?\n')
REGEX_ESCAPE_NEWLINE_2 = re.compile('\r')
def escape_newline(string):
    return REGEX_ESCAPE_NEWLINE_2.sub('\r\n',
        REGEX_ESCAPE_NEWLINE_1.sub('\r', string))

def onekey_extract(filepath, target_dir):
    if tarfile.is_tarfile(filepath):
        with tarfile.open(filepath) as opened:
            opened.extractall(path=target_dir)
    elif zipfile.is_zipfile(filepath):
        with zipfile.ZipFile(filepath) as opened:
            opened.extractall(path=target_dir)
        return True
    else:
        return None