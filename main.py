from config import get_config
from dal import initialize_data_access_layer, \
    check_main_session, check_user_password, \
    new_user, login_main, logout_main, \
    docker_get_container_iostream
initialize_data_access_layer(get_config())
from app import app, ws, sockets
import backstage
import api
from flask import render_template, request, url_for, redirect, Blueprint
from flask_sockets import Sockets

import threading
import datetime
import time

from logutil import log
from util import escape_newline

@app.route('/')
def main():
    return render_template(
        'main/index.html.jinja',
        # 'new_frontend/index.html',
        logged_in=check_main_session(request.cookies.get('username'), request.cookies.get('session_id')),
        username=request.cookies.get('username'),
        config=get_config()
    )

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        return render_template(
            'main/signup.html.jinja',
            config=get_config()
        )
    else:
        provided_username, provided_password = \
            request.form['username'], request.form['password']
        new_user(provided_username, provided_password)
        return render_template(
            'main/signup.html.jinja',
            actionmsg='Register success.',
            actionindicate=('Back', url_for('main')),
            config=get_config()
        )

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template(
            'main/login.html.jinja',
            logged_in=check_main_session(
                request.cookies.get('username'),
                request.cookies.get('session_id')
            ),
            config=get_config()
        )
    else:
        provided_username, provided_password = \
            request.form['username'], request.form['password']
        check_res = check_user_password(provided_password, username=provided_username)
        if check_res:
            session_id = login_main(provided_username)
            resp = redirect(url_for('main'))
            resp.set_cookie('username', provided_username)
            resp.set_cookie('session_id', session_id)
            return resp
        else:
            return render_template(
                'main/login.html.jinja',
                actionmsg='Login failed.',
                config=get_config()
            )

@app.route('/logout')
def logout():
    resp = redirect(url_for('main'))
    logout_main(request.cookies.get('username'))
    resp.delete_cookie('username')
    resp.delete_cookie('session_id')
    return resp
 
@app.route('/download/<username>/<session_id>')
def download():
    ...
    return ''

THREAD = None
THREAD_LOCK = threading.Lock()
TO_CONTAINER_SOCKET = None
FROM_CONTAINER_STREAM = None


@ws.route('/terminal')
def terminal_socket(socket):
    while not socket.closed:
        socket.send('3No connected sandbox.\r\n')
        msg = socket.receive()
        log(f'ws msg rcvd {msg}')
        while msg[0] != '1':
            msg = socket.receive()
        socket.send(
            '3Sandbox request received. Connecting...\r\n')
        # now we connect to the docker container.
        global FROM_CONTAINER_STREAM, TO_CONTAINER_SOCKET, THREAD, THREAD_LOCK
        CONTAINER, FROM_CONTAINER_STREAM, TO_CONTAINER_SOCKET = docker_get_container_iostream(msg[1:])
        if CONTAINER.status != 'running':
            socket.send('3Starting sandbox...\r\n')
            CONTAINER.start()
        STOPPED = False
        def sendthread():
            global FROM_CONTAINER_STREAM
            nonlocal socket, STOPPED
            while (not STOPPED) and (not socket.closed):
                try:
                    a = next(FROM_CONTAINER_STREAM)
                    log(f'sent {a}')
                    socket.send(f'2{escape_newline(a.decode())}')
                except StopIteration:
                    STOPPED = True
                    break
        threading.Thread(target=sendthread).start()
        try:
            while not STOPPED:
                # TODO: 这里阻塞了之后不能及时判断有没有STOPPED。改成不阻塞的。
                msg = socket.receive()
                if msg[0] == '0':
                    socket.send('0')
                elif msg[0] == '2':
                    msg = msg[1:]
                    log(f'rcvd {repr(msg)}')
                    if msg == '\r':
                        msg = '\n'
                    TO_CONTAINER_SOCKET._sock.send(msg.encode())
        except:
            pass
        socket.send('3Stopped.')
        socket.send('4')


def timeservice(socket):
    while True:
        time.sleep(1)
        socket.send(str(datetime.datetime.now()))


sockets.register_blueprint(ws, url_prefix=r'/ws')


if __name__ == "__main__":
    print("!!")
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    server.serve_forever()
