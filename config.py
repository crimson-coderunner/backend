
import os
import logging
import json

def get_supported_languages():
    ...
    # x = os.scandir('./config/containers')
    # for i in x:
    #     if not os.path.isfile(f'./config/containers/{i}/Dockerfile'):
    #         raise Exception('')


with open('./config/config.json', 'r') as config_file:
    _config_str = ''.join(config_file.readlines())
CONFIG = json.loads(_config_str)

def update_config():
    with open('./config/config.json', 'w') as config_file:
        config_file.write(json.dumps(CONFIG, indent=4))

def get_config():
    return CONFIG

def set_config(config_type, dic):
    global CONFIG
    CONFIG[config_type].update(dic)
    update_config()

def update_admin_user(username, password):
    global CONFIG
    CONFIG['admin-list'][username] = password
    update_config()

def remove_admin_user(username):
    global CONFIG
    del CONFIG['admin-list'][username]
    update_config()
