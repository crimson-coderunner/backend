coderunner.server.api doc
=========================

## 使用方式

+ HTTP POST
+ `[服务器地址]/api`
+ 需要将请求打包成JSON字符串并附在`msg` field。

Javascript示例：

``` javascript
function APIRequest(type, content, credential) {
    return new Promise((resolve, reject) => {
        // 为什么要用Promise？因为Javascript的执行一般都要比网络请求快，
        // 需要用Promise里处理「当数据确实得到手了」的情况。如：
        //     a = new Promise(请求).then((result) => {
        //         // 这里只有在数据到达之后才会执行
        //     }).catch((reason) => {
        //         // 这里只有在请求出错的时候才会执行
        //     })
        //     // 这里在new Promise之后就会立即执行。
        // XMLHttpRequest不会阻塞，所以为了保证能在正确的时间
        // 处理数据，需要Promise。
        var url = 'http://127.0.0.1:8000/api';
        var req = new XMLHttpRequest();
        var resp = null;
        req.addEventListener('error', function () {
            alert('Failed with reason (' + req.status + ') ' + req.statusText);
            reject({
                errcode: req.status,
                errmsg: req.statusText
            });
        });
        req.addEventListener('load', function () {
            resp = JSON.parse(req.responseText);
            if (resp.type === 'error') {
                alert('Failed with reason ' + resp.content.msg);
                reject({
                    errcode: -1,
                    errmsg: resp.content.msg
                });
            } else {
                resolve(resp);
            }
        });
        var data = new FormData();
        data.append('msg', JSON.stringify(
            {
                type: type,
                credential: credential,
                content: content
            }
        ));
        req.open('POST', url);
        req.send(data);
    });
}
// 然后：
APIRequest(type, content, credential)
.then((result) => {
    // ...
})
.catch((error) => {
    // ...
})
```

## 返回结果

返回结果只有两种：成功的和失败的。

成功的格式如下：
``` json
{
    "type": "ok",
    "content": "[返回内容]"
}
```

## 命令一览

### 注册

``` js
type = 'singup';
content = {
    username: '[用户名]',
    password: '[密码]'
}
```

### 登录

``` js
type = 'login';
content = {
    "username": "[这里填用户名]",
    "password": "[这里填密码]"
}
```

成功时返回content：

``` json
{
    "session_id": "[登录生成的session id]"
}
```

登录之后需要立即设cookie：
+ `username`为用户名
+ `session_id`为返回的session id

### 登出

``` js
type = 'logout';
content = {};
```

### 检查session_id是否合法

``` js
type = 'integrity';
content = {
    username: '[待检查的username]',
    session_id: '[待检查的session_id]'
}
```

### 获取支持的操作

``` js
type = 'lang';
content = {
    action: 'load-menu',
    lang: 'Java',
}
```

### 文件树操作

这一类操作需要附加`credential`，格式如下：
``` js
credential = {
    username: '[已登录的用户名]',
    session_id: '[登录时返回的session_id]'
}
```

#### 新建文件

``` js
type = 'file';
content = {
    "action": "new",
    "name": "[用户名]",
    "path": "[新建文件地址]"
}
```

#### 新建文件夹

``` js
type = "file";
content = {
    "action": "new-folder",
    "name": "[用户名]",
    "path": "[新建文件地址]"
}
```

#### 返回文件树

``` json
{
    "type": "file",
    "content": {
        "action": "list",
        "path": "[路径]"
    }
}
```

##### 删除文件

``` json
{
    "type": "file",
    "content": {
        "action": "delete",
        "path": "[文件路径]"
    }
}
```

##### 更新文件

``` js
type = 'file';
content = {
    action: 'update',
    path: '[文件路径]',
    content: '[新的文件内容]'
}
```

##### 获取文件内容

``` js
type = 'file';
content = {
    action: 'get',
    path: '[文件路径]',
}
```

#### 运行

``` js
type = 'sandbox';
```

##### 执行命令

``` js
content = {
    action: 'allocate',
    lang: '语言', // 目前是Java
    group: '组', // Project/Build/Run
    cmd: '操作id',
    /// 以下三个是可选的。不同的操作会需要不同的参数，
    // 例如「运行单个文件」就只需要filename不需要其他参数。
    filename: '文件名',
    dirname: '目录名',
    additional: {
        // 额外环境变量
    }
}
```

将返回一个`container_name`，需要将这个container_name用WebSocket发往`ws://134.175.150.51:8000/terminal`。

### 终端连接

需要WebSocket。

信息格式：类型+内容，例如，类型为2内容为`blah`的，就是`2blah`。


发送信息：

+ 类型0 - 隔一段时间往服务器端发一个0即可（一般为2秒）
+ 类型1 - 连接docker container，内容为API请求返回的container_name。会让WS服务器端与运行的沙盒对接起来并发送沙盒中显示的内容。
+ 类型2 - 用户的输入。内容为用户输入。会将内容发送到沙盒。

接收信息：

+ 类型0 - 服务器对类型0的信息的回复
+ 类型2 - 沙盒的输出。内容为沙盒中显示的内容。
+ 类型3 - 沙盒状态信息。内容为信息本身。
+ 类型4 - 沙盒运行停止的通知。


