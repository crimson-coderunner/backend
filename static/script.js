// most of this are copied from http://www.w3school.com.cn/js/js_cookies.asp

// Normal: style="color:#000000;background-color:#ffffff"
// Urban Dusk:  style="color:#ffee22;background-color:#000033"
// Voice of Autumn: style="color:#dd6600;background-color:#fffefe"
// Late Night Hacker: style="color:#00ff00;background-color:#000000"
// At Night: style="color:#ffffff;background-color:#000000"

THEME = {
    "": {
        "color": "#000000",
        "background-color": "#ffffff"
    },
    "normal": {
        "color": "#000000",
        "background-color": "#ffffff"
    },
    "urbandusk": {
        "color": "#ffee22",
        "background-color": "#000033"
    },
    "voiceofautumn": {
        "color": "#dd6600",
        "background-color": "#fffefe"
    },
    "latenighthacker": {
        "color": "#00ff00",
        "background-color": "#000000"
    },
    "atnight": {
        "color": "#ffffff",
        "background-color": "#000000"
    }
};

function getExpireDate() {
    let exdate = new Date();
    exdate.setDate(exdate.getDate() + 365);  // default to 365 days.
    return exdate;
}

function setCookie(key, value, expiredate) {
    document.cookie = key + "=" + escape(value) + ";expires=" + expiredate.toGMTString();
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=") + c_name.length + 1;
        c_end = document.cookie.indexOf(";", c_start);
        if (c_end == -1) c_end = document.cookie.length;
        return unescape(document.cookie.substring(c_start, c_end));
    }
    return "";
}

function themeswitch(x) {
    setCookie('theme', x, getExpireDate());
    refreshTheme();
}

function refreshTheme() {
    let themestr = getCookie("theme");
    domelem = document.getElementsByTagName("body")[0];
    let color = THEME[themestr]['color'];
    let bgcolor = THEME[themestr]['background-color'];
    domelem.style.color = color;
    domelem.style.backgroundColor = bgcolor;          
}