function getExpireDate() {
    let exdate = new Date();
    exdate.setDate(exdate.getDate() + 365);  // default to 365 days.
    return exdate;
}

function setCookie(key, value, expiredate) {
    document.cookie = key + "=" + escape(value) + ";expires=" + expiredate.toGMTString();
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=") + c_name.length + 1;
        c_end = document.cookie.indexOf(";", c_start);
        if (c_end == -1) c_end = document.cookie.length;
        return unescape(document.cookie.substring(c_start, c_end));
    }
    return "";
}

let GLOBAL = {};
GLOBAL.editor = null;
GLOBAL.terminal = null;
GLOBAL.dirtree = {};
GLOBAL.dirtree.selected_obj = null;
GLOBAL.constants = {};
GLOBAL.constants.convert = {
    'py': 'python',
    'java': 'java',
    'js': 'javascript',
    'txt': 'plain_text',
    'php': 'php',
    'html': 'html',
    'htm': 'html',
    'c': 'c_cpp',
    'h': 'c_cpp',
    'cpp': 'c_cpp',
    'hpp': 'c_cpp',
    'css': 'css',
    'cs': 'csharp',
    'go': 'golang',
    'hs': 'haskell',
    'json': 'json',
    'jl': 'julia',
    'tex': 'latex',
    'lua': 'lua',
    'md': 'markdown',
    'pas': 'pascal',
    'r': 'r',
    'rst': 'rst',
    'rb': 'ruby',
    'scm': 'scheme',
    'sql': 'sql',
    'xml': 'xml',
    'yaml': 'yaml',
    'sh': 'sh',
};

function API(type, content, callback) {
    let data = new FormData();
    content.name = getCookie('username');
    data.append('msg', JSON.stringify({
        'type': type,
        'content': content
    }));
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            callback(req.responseText);
        }
    }
    req.open('POST', '/api', true);
    req.send(data);
}

function getUserDirTree() {
    API('file', {
        'action': 'list',
        'path': '/'
    }, function(resptext) {
        displayUserDirTree(resptext);
    });
}

function setDirEntryStyle(obj) {
    obj.style.borderTop = '1px #000000 solid';
    obj.style.paddingLeft = '16px';
}
function setSelected(obj) {
    obj._selected = true;
    obj.firstChild.style.backgroundColor = '#dddddd';
}
function clearSelected(obj) {
    obj._selected = false;
    obj.firstChild.style.backgroundColor = '#ffffff';
}
function toggleSelected(obj) {
    obj._selected = !obj._selected;
    obj.firstChild.style.backgroundColor = obj._selected? '#dddddd' : '#ffffff';
    if (obj._parent_obj !== null) {
        (obj._selected? setSelected : clearSelected)(obj._parent_obj);
    }
}
function selectEntry(obj) {
    if (GLOBAL.dirtree.selected_obj !== null) {
        toggleSelected(GLOBAL.dirtree.selected_obj);
    }
    toggleSelected(obj);
    GLOBAL.dirtree.selected_obj = obj;
    document.getElementById('dirtree_pathindicator').innerHTML =
        obj._is_file? obj._parent_path : obj._path;
}

function alert_when_error(resptext) {
    let respobj = JSON.parse(resptext);
    if (respobj.type === 'error') {
        alert('Action failed with msg: ' + respobj.content.msg);
    }
}

// parentobj must be a Node containing an entry node and optionally
// an childlist node.
function _mkdirentry(is_file, parentobj, parentpath, name) {
    let wrapper = document.createElement('div');
    // 1. set up informations.
    wrapper.setAttribute('id', 'dirtree_' + parentpath + '/' + name);
    wrapper._parent_path = parentpath;
    wrapper._parent_obj = parentobj;
    wrapper._path = parentpath + '/' + name;
    wrapper._selected = false;
    wrapper._is_file = is_file;
    wrapper.select = function() {
        selectEntry(this);
    }
    wrapper.addChildEntry = function(x) {
        wrapper.lastChild.appendChild(x);
    }
    wrapper.clearChildEntry = function() {
        if (!wrapper._is_file) {
            while (wrapper.lastChild.hasChildNodes()) {
                wrapper.lastChild.removeChild(wrapper.lastChild.lastChild);
            }
        }
    }
    
    // 2. clickable entry.
    let elem = document.createElement('div');
    elem.setAttribute('id', 'dirtree_' + parentpath + '/' + name + '_elem');
    let icon = document.createElement('img');
    icon.setAttribute(
        'src',
        'static/assets/' + (is_file ? 'file' : 'folder') + '.svg'
    );
    elem.appendChild(icon);
    elem.appendChild(document.createTextNode(name));
    elem.ondblclick = function (e) {
        wrapper.select()
        if (is_file) {
            loadFile(wrapper._path);
        }
    }
    wrapper.appendChild(elem);

    // 3. childlist node when it's a directory.
    if (!is_file) {
        let elem2 = document.createElement('div');
        elem2.setAttribute('id', 'dirtree_' + parentpath + '/' + name + '_child');
        setDirEntryStyle(elem2);
        wrapper.appendChild(elem2);
    }

    // 4. set style

    return wrapper;

}

function setActionMsg(msg) {
    document.getElementById('actionmsg').innerHTML = msg;
}

function displayUserDirTree(resp) {
    function _display(parentobj, parentpath, obj) {
        for (childpath in obj) {
            let entry = _mkdirentry(obj[childpath] === null, parentobj, parentpath, childpath);
            parentobj.addChildEntry(entry);
            if (obj[childpath] !== null) {
                _display(entry, parentpath + '/' + childpath, obj[childpath]);
            }
        }
    }
    let tree = JSON.parse(resp);
    let dirtree = _mkdirentry(false, null, '.', '.');
    _display(dirtree, '.', tree.content.result);
    let dirtreewrapper = document.getElementById('dirtree');
    while (dirtreewrapper.hasChildNodes()) {
        dirtreewrapper.removeChild(dirtreewrapper.lastChild);
    }
    let pathIndicator = document.createElement('div');
    pathIndicator.style.backgroundColor = '#000000';
    pathIndicator.style.color = '#ffffff';
    pathIndicator.setAttribute('id', 'dirtree_pathindicator');
    pathIndicator.innerHTML = '.';
    dirtree.firstChild.ondblclick = null;
    dirtreewrapper.appendChild(pathIndicator);
    dirtreewrapper.appendChild(dirtree);
}

function loadFile(path) {
    // 1. get file content.
    API('file', {
        'action': 'get',
        'path': path
    }, function (resptext) {
        let resp = JSON.parse(resptext);
        let mres = path.match(/\.([^\/.]*?)$/);
        let postfix = mres? path.match(/\.([^\/.]*?)$/)[1] : 'txt';
        GLOBAL.editor.session.setMode('ace/mode/' + GLOBAL.constants.convert[postfix]);
        GLOBAL.editor.setValue(resp.content.content);
    })
}

function saveFile() {
    let path = GLOBAL.dirtree.selected_obj._path;
    API('file', {
        'action': 'update',
        'path': path,
        'content': GLOBAL.editor.getValue()
    }, function (resptext) {
        alert_when_error(resptext);
        alert('Saved.');
    });
}

function newFile() {
    let filename = prompt('File name:');
    let parentpath =
        GLOBAL.dirtree.selected_obj?
            (GLOBAL.dirtree.selected_obj._is_file?
                GLOBAL.dirtree.selected_obj._parent_path
                : GLOBAL.dirtree.selected_obj._path)
            : '.';
    API('file', {
        'action': 'new',
        'filename': filename,
        'path': parentpath
    }, function (resptext) {
        alert_when_error(resptext);
        alert('File created');
    });
    getUserDirTree();
}

function deleteFile() {
    let filename = GLOBAL.dirtree.selected_obj._path;
    let confirmRes = confirm('Do you really want to delete ' + filename + '?');
    if (confirmRes) {
        API('file', {
            'action': 'delete',
            'path': filename
        }, function (resptext) {
            alert_when_error(resptext);
            alert('Done.');
        });
        getUserDirTree();
    }
}

function newFolder() {
    let filename = prompt('Folder name:');
    let parentpath =
        GLOBAL.dirtree.selected_obj?
            (GLOBAL.dirtree.selected_obj._is_file ?
                GLOBAL.dirtree.selected_obj._parent_path
                : GLOBAL.dirtree.selected_obj._path)
            : '.';
    API('file', {
        'action': 'new-folder',
        'foldername': filename,
        'path': parentpath
    }, function (resptext) {
        alert_when_error(resptext);
        alert('Folder created');
    });
    getUserDirTree();
}

function runFile() {
    let filename = GLOBAL.dirtree.selected_obj._path;
    let mres = filename.match(/\.([^\/.]*?)$/);
    let postfix = mres ? filename.match(/\.([^\/.]*?)$/)[1] : 'txt';
    let lang = GLOBAL.constants.convert[postfix];
    setActionMsg('Waiting for result...');
    API('sandbox', {
        'action': 'allocate',
        'lang': lang,
        'filename': filename
    }, function (resptext) {
        resp_obj = JSON.parse(resptext);
        GLOBAL.terminal.fit();
        GLOBAL.terminal.clear();
        connectTerminal(resp_obj.content.container_name);
        setActionMsg('Container name received.');
        API('sandbox', {
            'action': 'connect',
            'container_name': resp_obj.content.container_name
        }, function () {})
    });
}

function connectTerminal(container_name) {
    GLOBAL.socket.emit('USE', { 'container_name': container_name });
}

