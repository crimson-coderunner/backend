from logutil import log

DESIGNATED_CONTAINER = None
SUBSCRIBER = []

def set_designated_container(container_name):
    global DESIGNATED_CONTAINER
    DESIGNATED_CONTAINER = container_name
    log(f'RUNNER_STATE>> designated: {container_name}')

def subscribe(sub_f):
    log(f'RUNNER_STATE>> subscribing {str(sub_f)}')
    SUBSCRIBER.append(sub_f)

def notify():
    for x in SUBSCRIBER:
        log(f'RUNNER_STATE>> notifying {str(x)}')
        x()

