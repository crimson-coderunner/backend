# data access layer.
import os
import json
import shutil
import base64
import redis
import docker
import threading
import mysql.connector
from pathlib import PurePath

import util
from util import password_generate_hash, password_check_hash, align_name, append_random_postfix
from config import get_config
from logutil import log

_INITIALIZED = False
REDIS_SERVER = None
MYSQL_SERVER = None
DOCKER_CLIENT = None

# TODO: make it use the with sentence.

def _initMysql(config):
    global MYSQL_SERVER
    MYSQL_SERVER = mysql.connector.connect(
        host=config['mysql']['hostname'],
        port=config['mysql']['portno'],
        user=config['mysql']['db_username'],
        password=config['mysql']['db_password'],
        database=config['mysql']['db_name'],
    )
def _initRedis(config):
    global REDIS_SERVER
    REDIS_SERVER = redis.Redis(
        host=config['redis']['hostname'],
        port=config['redis']['portno']
    )
def _initDocker(config):
    global DOCKER_CLIENT
    DOCKER_CLIENT = docker.from_env()

def initialize_data_access_layer(config):
    global _INITIALIZED, REDIS_SERVER, DOCKER_CLIENT, MYSQL_SERVER
    if not _INITIALIZED:
        _initRedis(config)
        _initMysql(config)
        _initDocker(config)
        _INITIALIZED = True

def _withMysql(query, args=None, req_res=False):
    _retry = 0
    while True:
        try:
            cursor = MYSQL_SERVER.cursor(buffered=True)
            if args:
                cursor.execute(query, args)
            else:
                cursor.execute(query)
            MYSQL_SERVER.commit()
            res = True
            if req_res:
                res = cursor.fetchall()
                log(f'fetched query {query} with args {str(args)}')
            cursor.close()
            return res
        except Exception as e:
            log(f'MYSQL ERROR: {str(e.args)} with {_retry} retry')
            if _retry > 5:
                log(f'failed after {_retry} retry. leaving.')
                break
            _initMysql(get_config())
            _retry += 1


def _mkuservolumename(username):
    return f'coderunner_vol_{username}'

def _mkadminsessionkey(admin_name, session_type):
    return f'coderunner:session:{session_type}:{admin_name}'

def _mkmainsessionkey(username):
    return f'coderunner:session:login:{username}'

def _mkdownloadsessionkey(username, filepath):
    return f'coderunner:session:download:{username}:{filepath}'


def start_download_session(username, filepath):
    generated_downloadsession_id = base64.b64encode(os.urandom(64))
    REDIS_SERVER.set(_mkdownloadsessionkey(username, filepath), generated_downloadsession_id)
    return generated_downloadsession_id

def check_backstage_session(admin_name, session_id):
    if admin_name and session_id:
        r = REDIS_SERVER.get(_mkadminsessionkey(admin_name, 'backstage'))
        if not r:
            return False
        r = r.decode()
        return r == session_id
    else:
        return False

def login_backstage(admin_name):
    generated_session_id = base64.b64encode(os.urandom(32))
    REDIS_SERVER.set(_mkadminsessionkey(admin_name, 'backstage'), generated_session_id, ex=300)
    return generated_session_id

def logout_backstage(admin_name):
    REDIS_SERVER.delete(_mkadminsessionkey(admin_name, 'backstage'))

def login_main(username):
    generated_session_id = base64.b64encode(os.urandom(32))
    REDIS_SERVER.set(_mkmainsessionkey(username), generated_session_id)
    return generated_session_id

def logout_main(username):
    REDIS_SERVER.delete(_mkmainsessionkey(username))

def check_main_session(username, session_id):
    if username and session_id:
        r = REDIS_SERVER.get(_mkmainsessionkey(username))
        if not r:
            return False
        r = r.decode()
        return r == session_id
    else:
        return False


## docker related stuff...

def docker_save_dockerfile(lang_name, werkzeug_fileobj):
    dockerfile_storage = get_config()['sandbox']['default_dockerfile_storage']
    residence_path = os.path.join(dockerfile_storage, lang_name)
    if not os.path.exists(residence_path):
        os.makedirs(residence_path)
    werkzeug_fileobj.save(os.path.join(residence_path, 'Dockerfile'))
    langjson = docker_langjson_load()
    langjson[f'{lang_name}'] = None
    docker_langjson_save(langjson)


def docker_langjson_load():
    dockerfile_storage = get_config()['sandbox']['default_dockerfile_storage']
    with open(os.path.join(dockerfile_storage, 'lang.json'), 'r') as f:
        lang_json = ''.join(f.readlines())
    return json.loads(lang_json)

def docker_langjson_save(obj):
    dockerfile_storage = get_config()['sandbox']['default_dockerfile_storage']
    with open(os.path.join(dockerfile_storage, 'lang.json'), 'w') as f:
        f.write(json.dumps(obj, indent=4))

def docker_mkvolume(username):
    name = append_random_postfix(f'coderunner_{username}_')
    log(name)
    DOCKER_CLIENT.volumes.create(
        name=name,
        labels={
            'type': 'coderunner',
            'user': username
        }
    )
    return name

def lang_new(lang_name):
    langjson = docker_langjson_load()
    if lang_name not in langjson:
        langjson[lang_name] = {'Project': {}, 'Build': {}, 'Run': {}}
    docker_langjson_save(langjson)

def lang_get_action(lang):
    langjson = docker_langjson_load()
    if lang not in langjson:
        raise Exception(f'不支持的语言{lang}')
    group_conf = langjson[lang]
    def _rm_docker_image(langdict):
        return {x:langdict[x] for x in langdict if x != 'image'}
    return {group_name:[_rm_docker_image(item) for item in group_conf[group_name]] for group_name in group_conf}

def lang_get_action_path(lang, group, action_name):
    dockerfile_storage = get_config()['sandbox']['default_dockerfile_storage']
    shouldbe_path = PurePath(dockerfile_storage, lang, group, action_name)
    return shouldbe_path

def langjson_new_action(lang, group, action_name, name):
    # 1. update langjson.
    langjson = docker_langjson_load()
    if lang not in langjson:
        raise Exception(f'不支持的语言{lang}')
    if group not in langjson[lang]:
        langjson[lang][group] = {}
    if action_name not in langjson[lang][group]:
        langjson[lang][group][action_name] = {}
    langjson[lang][group][action_name]['name'] = name
    docker_langjson_save(langjson)

    # 2. make dir.
    shouldbe_path = lang_get_action_path(lang, group, action_name)
    if not os.path.exists(shouldbe_path):
        os.makedirs(shouldbe_path)    

def lang_save_action_package(lang, group, action_name, werkzeug_fileobj):
    action_path = lang_get_action_path(lang, group, action_name)
    target_path = PurePath(action_path, 'bundle')
    werkzeug_fileobj.save(str(target_path))
    util.onekey_extract(target_path, action_path)
    os.remove(target_path)

    # 2. add action.json into lang.json
    action_json_path = PurePath(action_path, 'action.json')
    if not os.path.exists(action_json_path):
        raise Exception('没有操作描述文件')
    with open(action_json_path, 'r') as f:
        action_json = f.read()
    action_json = json.loads(action_json)
    # 2.1 load langjson.
    langjson = docker_langjson_load()
    langjson[lang][group][action_name].update(action_json)
    docker_langjson_save(langjson)

def lang_delete_action(lang, group, action_id):
    # 1. remove action from langjson.
    langjson = docker_langjson_load()
    del langjson[lang][group][action_id]
    docker_langjson_save(langjson)

    # 2. remove action package.
    shutil.rmtree(lang_get_action_path(lang, group, action_id))

def docker_get_server_info():
    return DOCKER_CLIENT.info()

def docker_get_container_list(list_all=True):
    return DOCKER_CLIENT.containers.list(all=list_all)

def docker_get_image_list(list_all=True):
    return DOCKER_CLIENT.images.list(all=list_all)

def docker_get_container(container_name=None, container_id=None):
    if not (container_name or container_id):
        raise Exception('No container name/id for retrieving')
    return DOCKER_CLIENT.containers.get(container_id if container_id else container_name)

def docker_run(
    username,
    lang=None, group=None, action=None,
    filepath=None, dirpath=None, additional=None
):
    if not lang:
        raise Exception('No language specified.')
    if not filepath:
        raise Exception('No file specified.')
    langjson = docker_langjson_load()
    log('creating container...')
    # prepare the env vars.
    env_var = []
    if filepath:
        env_var.append(f'CODERUNNER_TARGET_FILE={filepath}')
    if dirpath:
        env_var.append(f'CODERUNNER_TARGET_DIR={dirpath}')
    if additional:
        for key in additional:
            env_var.append(f'{key}={additional[key]}')
    user = get_user(username=username)
    volname = user['volume_id']
    container = DOCKER_CLIENT.containers.create(
        langjson[lang][group][action]['image'],
        mounts=[
            docker.types.Mount(
                '/workspace',
                volname,
            )
        ],
        environment=env_var,
        stdin_open=True
        # ,auto_remove=True
    )
    log(f'sandbox {lang}.{group}.{action}.{langjson[lang][group][action]["image"]} allocated {container.name}')
    return container.name

def docker_build(lang_name, group, action):
    def _build_thread():
        langjson = docker_langjson_load()
        name = align_name('', length=16, random_choice='abcdefghijklmnopqrstuvwxyz0123456789_')
        for line in DOCKER_CLIENT.api.build(
            path=f'{get_config()["sandbox"]["default_dockerfile_storage"]}/{lang_name}/{group}/{action}',
            tag=f'coderunner_{name}',
            labels={'lang': lang_name, 'group': group, 'action': action},
            rm=True
        ):
            log(f'docker build {line.decode()}')
        langjson[lang_name][group][action]['image'] = f'coderunner_{name}'
        docker_langjson_save(langjson)
    threading.Thread(target=_build_thread).start()

def docker_buildall():
    log(f"docker_buildall>> build all started.")
    langjson = docker_langjson_load()
    for lang_name in langjson:
        for group in langjson[lang_name]:
            for action in langjson[lang_name][group]:
                docker_build(lang_name, group, action)
    log(f"docker_buildall>> build all complete.")

def docker_get_container_iostream(container_name):
    container = docker_get_container(container_name=container_name)
    return (
        container,
        container.attach(stdout=True, stderr=True, stream=True),
        container.attach_socket(params={'stdin': True, 'stream': True})._sock
    )

## file related

def get_user_workspace(username):
    user = get_user(username=username)
    vol = DOCKER_CLIENT.volumes.get(user['volume_id'])
    return PurePath(vol.attrs['Mountpoint'])

def list_path(username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid provided for retrieving dirtree.')
    if userid:
        username = get_user(userid=userid)['uname']
    def _rec(current_path, ignoring=['.git', '__pycache__']):
        return dict( \
            (path.name, _rec(path) if path.is_dir() else None) \
                for path in os.scandir(path=current_path) \
                if path.name not in ignoring
        )
    return _rec(get_user_workspace(username))

def can_read(username, path):
    return os.access(PurePath(get_user_workspace(username), path), os.R_OK)

def can_write(username, path):
    return os.access(PurePath(get_user_workspace(username), path), os.W_OK)

def new_file(username, dirpath, filename):
    # NOTE: 应该有更好的做法。
    path = PurePath(get_user_workspace(username), dirpath, filename)
    log(f'new file creating at {str(path)}')
    if not os.path.exists(path.parent):
        os.makedirs(path.parent)
    with open(path, 'x'):
        pass

def new_folder(username, dirpath, filename):
    # NOTE: 应该有更好的做法。
    path = PurePath(get_user_workspace(username), dirpath, filename)
    log(f'new folder creating {str(path)}')
    if not os.path.exists(path):
        os.makedirs(path)

def delete_file(username, filepath):
    # TODO: 将这个换成正常判断是否目录的做法。
    try:
        path = PurePath(get_user_workspace(username), filepath)
        log(f'try to delete {str(path)}')
        os.remove(path)
    except IsADirectoryError:
        shutil.rmtree(PurePath(get_user_workspace(username), filepath))

def update_file(username, filepath, content):
    path = PurePath(get_user_workspace(username), filepath)
    log(f'update file {str(path)}')
    with open(path, 'w') as f:
        f.write(content)

def get_file(username, filepath):
    path = PurePath(get_user_workspace(username), filepath)
    with open(path, 'rb') as f:
        res = f.read()
    try:
        res = res.decode('utf-8')
        return (True, res)
    except:
        res = res.decode('latin1')
        return (False, res)
    return res

def rename_file(username, inhabitPath, oldname, newname):
    oldpath = PurePath(get_user_workspace(username), inhabitPath, oldname)
    newpath = PurePath(get_user_workspace(username), inhabitPath, newname)
    os.rename(oldpath, newpath)

## mysql related

def to_user(uid, uname, upass, volume_id, blocked):
    return {
        'uid': uid,
        'uname': uname,
        'upass': upass,
        'volume_id': volume_id,
        'blocked': blocked
    }

def new_user(username, userpass):
    # 如果存在用户就不注册了。
    test_result = get_user(username=username)
    if test_result:
        raise Exception('User existed')
    hashed_pass = password_generate_hash(userpass)
    # create volume.
    volname = docker_mkvolume(username)
    _withMysql(
        "INSERT INTO Users(uname, upass, volume_id, blocked) VALUE (%s, %s, %s, %s)",
        args=(username, hashed_pass, volname, 0)
    )

def set_block_user(block=True, username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid provided for blocking.')
    _withMysql(
        f"UPDATE Users SET blocked = %s WHERE {'uid' if userid else 'uname'} = %s",
        args=(1 if block else 0, userid if userid else username,)
    )

def toggle_user_ban(username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid provided for blocking.')
    _withMysql(
        f"UPDATE Users SET blocked = 1 - blocked WHERE {'uid' if userid else 'uname'} = %s",
        args=(userid if userid else username,)
    )

def remove_user(username=None, userid=None):
    # we have to first remove the volume...
    user = get_user(username=username, userid=userid)
    DOCKER_CLIENT.volumes.get(user['volume_id']).remove(True)

    if not (username or userid):
        raise Exception('No username/userid provided for deleting.')

    _withMysql(
        f"DELETE FROM Users WHERE {'uid' if userid else 'uname'} = %s",
        args=(userid if userid else username,)
    )

def modify_user_password(username, old_pass, new_pass):
    oldpasshash = _withMysql(
        f"SELECT upass FROM Users WHERE uname = %s",
        args=(username,),
        req_res=True
    )
    if not oldpasshash:
        raise Exception(f'no registered password for {username}')
    oldpasshash = oldpasshash[0][0]
    check_res = password_check_hash(old_pass, oldpasshash)
    if check_res:
        _withMysql(
            f"UPDATE Users SET upass = %s WHERE uname = %s",
            args=(password_generate_hash(new_pass), username)
        )
    else:
        raise Exception('Wrong old password.')

def edit_user(d):
    if not ('uid' in d or 'uname' in d):
        raise Exception('No username/userid for editing')
    where_method = 'uid' if 'uid' in d else 'uname'
    # NOTE: 这是为了保证能够按固定的顺序来。
    ordered_key = [x for x in d if x not in ['uid', 'uname']]
    _withMysql(
        f"UPDATE Users SET {','.join([f'{x} = %s' for x in ordered_key])} WHERE {where_method} = {d[where_method]}",
        args=tuple(d[x] for x in ordered_key)
    )

def get_users():
    res = _withMysql(
        f"SELECT * FROM Users",
        req_res=True
    )
    return [to_user(*x) for x in res]

def get_user(username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid provided for querying')
    res = _withMysql(
        f"SELECT * FROM Users WHERE {'uid' if userid else 'uname'} = %s",
        args=(userid if userid else username,),
        req_res=True
    )
    if len(res) <= 0:
        return None
    return to_user(*res[0])

def check_user_banned(username):
    user = get_user(username=username)
    if user:
        return user['blocked']
    else:
        raise Exception(f'No user with username {username}')

def check_user_password(password, username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid provided for checking')
    res = _withMysql(
        f"SELECT upass FROM Users WHERE {'uid' if userid else 'uname'} = %s",
        args=(userid if userid else username,),
        req_res=True
    )
    return password_check_hash(password, res[0][0])

################################################# HIGHER LEVEL STUFF

def sandbox_get_iostream(username=None, userid=None):
    if not (username or userid):
        raise Exception('No username/userid for retrieving sandbox streams.')
    user = get_user(username=username, userid=userid)
    container = docker_get_container(container_name=user['sandbox_id'])
    return (
        container,
        container.attach(stdout=True, stderr=True, stream=True, logs=True),
        container.attach_socket(params={'stdin':True, 'stream':True})._sock
    )
