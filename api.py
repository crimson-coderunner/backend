import os
import json
from pathlib import PurePath

from flask import request, make_response

from app import app
from dal import \
    login_main, logout_main, check_user_password, get_user_workspace, \
    check_main_session, \
    list_path, new_file, can_read, can_write, delete_file, \
    update_file, get_file, new_folder, docker_run, \
    rename_file, \
    docker_langjson_load, \
    new_user, \
    check_user_banned
from logutil import log

def _respwrap(respstr):
    res = make_response(respstr)
    res.headers['Access-Control-Allow-Origin'] = '*'
    return res

def SUCCESS(content=None):
    return _respwrap(
        json.dumps({
            "type": "ok",
            "content": content
        } if content else {"type": "ok"})
    )
def FAIL(msg):
    return _respwrap(json.dumps({
        "type": "error",
        "content": {
            "msg": msg
        }
    }))
def WARNING(content=None):
    return _respwrap(
        json.dumps({
            "type": "warn",
            "content": content
        } if content else {"type": "warn"})
    )

@app.route('/api', methods=['POST'])
def api():
    log(f'get request {request.form}')

    # integrity check.
    if 'msg' not in request.form:
        return FAIL('Invalid request.')
    try:
        request_object = json.loads(request.form['msg'])
    except:
        return FAIL('Invalid request.')
    if not ('type' in request_object and 'content' in request_object):
        return FAIL('Invalid request.')

    content = request_object['content']

    ##################### SIGN UP
    if request_object['type'] == 'signup':
        try:
            new_user(
                request_object['content']['username'],
                request_object['content']['password']
            )
            return SUCCESS()
        except Exception as e:
            return FAIL(str(e.args[0]))

    ##################### LANG RELATED
    elif request_object['type'] == 'lang':
        content_obj = request_object['content']
        action = content_obj['action']
        if action == 'list_lang':
            return SUCCESS(list(docker_langjson_load().keys()))
        elif action == 'load_menu':
            if content_obj['lang'] != 'Plain_Text':
                return SUCCESS(docker_langjson_load()[content_obj['lang']])
            else:
                return SUCCESS()
        else:
            return FAIL('Invalid Request')

    ##################### LOGIN
    elif request_object['type'] == 'login':
        log(f'req type login')
        try:
            username, password = content['username'], content['password']
        except:
            return FAIL('No username/password for login.')
        if check_user_banned(username):
            return FAIL("You are banned from using the system. Please contact the webmaster.")
        if check_user_password(password, username=username):
            session_id = login_main(username).decode()
            resp = SUCCESS({"session_id": session_id})
            log(f'session_id for {username}: {session_id}')
            # CORS
            resp.headers['Access-Control-Allow-Origin'] = '*'
            resp.set_cookie('username', username)
            resp.set_cookie('session_id', session_id)
            return resp
        else:
            return FAIL("Username or password error.")

    ######################## LOGOUT
    elif request_object['type'] == 'logout':
        logout_main(content['username'])
        return SUCCESS()

    ######################## INTEGRITY
    elif request_object['type'] == 'integrity':
        content = request_object['content']
        if 'username' not in content or 'session_id' not in content:
            return FAIL('Invalid request.')
        log(f'INTEGRITY GET USERNAME: {content["username"]}')
        if check_user_banned(content['username']):
            return FAIL("You are banned from using the system. Please contact the webmaster.")
        res = check_main_session(content['username'], content['session_id'])
        log(f'INTEGRITY CHECK RES>> {res}')
        return SUCCESS({'result': res})

    ################################################
    #### USER-LOGIN REQUIRED ACTIONS
    else:
        credential_username, credential_sessionid = None, None
        if 'credential' in request_object:
            credential_username = request_object['credential']['username']
            credential_sessionid = request_object['credential']['session_id']
        else:
            credential_username, credential_sessionid = request.cookies.get('username'), request.cookies.get('session_id')

        if check_user_banned(credential_username):
            return FAIL("You are banned from using the system. Please contact the webmaster.")

        if not check_main_session(credential_username, credential_sessionid):
            return FAIL("Bad login credential or not logged in.")

        if request_object['type'] == 'file':
            action = content['action']
            username = credential_username
            path = content['path']
            if not path:
                path = get_user_workspace(username)
            if action == 'new':
                if not can_write(username, path):
                    return FAIL("Permission denied.")
                new_file(username, path, content['filename'])
                return SUCCESS()
            elif action == 'new-folder':
                log(f'get action newfolder for {path} {content["foldername"]}')
                if not can_write(username, path):
                    return FAIL("Permission denied.")
                new_folder(username, path, content['foldername'])
                return SUCCESS()
            elif action == 'rename':
                old_name, new_name = content['oldname'], content['newname']
                if not can_write(username, path):
                    return FAIL("Permission denied")
                rename_file(username, path, old_name, new_name)
                return SUCCESS()
            elif action == 'list':
                if not can_read(username, path):
                    return FAIL("Permission denied.")
                return SUCCESS({
                    "result": list_path(username)
                })
            elif action == 'delete':
                if not can_write(username, path):
                    return FAIL("Permission denied.")
                delete_file(username, path)
                return SUCCESS()
            elif action == 'update':
                if not can_write(username, path):
                    return FAIL("permission denied")
                update_file(username, path, content['content'])
                return SUCCESS()
            elif action == 'get':
                if not can_read(username, path):
                    return FAIL("Permission denied.")
                isutf8, content = get_file(username, path)
                if isutf8:
                    return SUCCESS({"content": content})
                else:
                    return WARNING({"content": content, "warnmsg": "Failed to decode as UTF-8 encoding. Are you opening a binary file?"})

        #################### RUN ACTIONS
        elif request_object['type'] == 'sandbox':
            req_content = request_object['content']
            action = req_content['action']
            if action == 'allocate':
                log('sandbox action allocate rcvd')
                lang = req_content['lang']
                group = req_content['group']
                cmd = req_content['command']
                filename = req_content['filename'] if 'filename' in req_content else None
                dirname = req_content['dirname'] if 'dirname' in req_content else None
                additional = req_content['additional'] if 'additional' in req_content else None
                username = credential_username
                if not can_read(username, filename):
                    return FAIL("Permission denied.")
                return SUCCESS({"container_name": docker_run(
                    username,
                    lang=lang, group=group, action=cmd,
                    filepath=filename, dirpath=dirname,
                    additional=additional
                )})
            else:
                return FAIL("Invalid API call.")
        else:
            return json.dumps({
                "type": "error",
                "content": {
                    "msg": "Unsupported usage."
                }
            })

