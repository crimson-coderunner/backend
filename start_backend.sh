#!/bin/bash

gunicorn --reload --log-file ./log.txt --capture-output -k flask_sockets.worker -b 0.0.0.0:8000 main:app &
