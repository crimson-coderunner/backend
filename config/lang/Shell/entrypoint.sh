#!/bin/bash

adduser -D -H -h /workspace -u 9999 user9999
export HOME=/workspace
mkdir /workspace/.coderunner_session
mv /wrapper.sh /workspace/.coderunner_session/wrapper.sh
chown -R user9999 /workspace
cd /workspace

exec /gosu-i386 user9999 sh ./.coderunner_session/wrapper.sh $CODERUNNER_TARGET_FILE
