#!/bin/sh

pathname=${CODERUNNER_TARGET_FILE%/*}
lenpath=${#pathname}
filename=${CODERUNNER_TARGET_FILE:1+lenpath}

cd /workspace/$pathname
/usr/lib/jvm/default-jvm/bin/javac $filename
java ${filename%.*}



rm /workspace/${filename%.*}.class

