#!/bin/bash

export HOME=/workspace
chown -R user9999 /workspace

cd /workspace

exec /gosu-i386 user9999 bash /wrapper.sh $CODERUNNER_TARGET_FILE $CODERUNNER_TARGET_DIR $CODERUNNER_GRADLE_ARGS